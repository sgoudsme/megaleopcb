 
# Arduino Mega MQTT breakout board

This project is a spinoff of the Universal plate. The device is used to control and read signals around the house. The signals are communicated over MQTT to the broker. This software is compliant to Home Assistant configurations. This project needs an Arduino Mega 2560 and an Ethernet shield W5100.
**IMPORTANT: The software needs a serial number written in EEPROM before uploading this code. See: [https://github.com/sgoudsme/Write-EEPROM-numbers](https://github.com/sgoudsme/Write-EEPROM-numbers).**

This project consists of two parts:
 - Hardware PCB design
 - Arduino Software

## Hardware
In the folder "Hardware", a PCB design is found. This is based on an Arduino Mega. It makes almost every pin accessible to connect wires. The signals are 12V compatible. A separate 5V power supply for the Arduino is also necessary.

List of connections:
- 16 Digital Output. The output voltage is 12V. Used for Relay controls. Can be connected with terminal blocks
- 4 times 6 Digital inputs. This can be connected with 4 DC3-8P SMD connectors. Signals are 12V compliant. (12V power output is also on this connector). These are used for wired switches in the house.
- 4 times 2 Digital and 2 Analog inputs. These pins are only 5V compatible. Used for motion sensors. (See: [https://gitlab.com/sgoudsme/motionbreakout](https://gitlab.com/sgoudsme/motionbreakout))
- 4 status leds (digital in/output, analog input)

## Software
Software is written in the Arduino IDE. A basic layout of output messages is `D1234/a/state`. Where the first part is the unique serial number of the arduino, the second part is the name of the pin and the last part is fixed.
For input messages, the layout is of form: `D1234/A/command` where only the last part changes to a fixed word.

The software is able to:

- Report 24 digital input pins (ASCII character a until x) (12V)
- Report 8 analog input pins (ASCII characters 0 until 7) (5V)
- Report 8 digital input pins (ASCII characters 8 until ?) (5V)
- Control 16 Digital Output pins (ASCII characters A until P) (
