
#include <Ethernet.h>
#include <SPI.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <avr/wdt.h>

#define GREEN_STATUS 55
#define BLUE_STATUS 54
#define STATUS_ON 0
#define STATUS_OFF 1

struct out_mapping
{
  char name;
  uint8_t pin_nr;
};

struct in_mapping_ana
{
  char name;
  uint8_t pin_nr;
  char prevVal;
  uint8_t cnt;
};

struct in_mapping_dig
{
  char name;
  uint8_t pin_nr;
  bool prevVal;
  bool activated;
};

#define OUT_MAPPING_LEN 16
struct out_mapping out_pin_mapping[OUT_MAPPING_LEN] = {
  {'A', 13},
  {'B', 12},
  {'C', 9},
  {'D', 11},

  {'E', 8},
  {'F', 7},
  {'G', 5},
  {'H', 6},

  {'I', 4},
  {'J', 3},
  {'K', 22},
  {'L', 2},

  {'M', 47},
  {'N', 46},
  {'O', 48},
  {'P', 49},
};

#define IN_MAPPING_ANA_LEN 6
struct in_mapping_ana in_pin_mapping_analog[IN_MAPPING_ANA_LEN] = {
  {'0', 9, 0, 0},
  {'1', 8, 0, 0},
  //{'2', 6, 0, 0},
  //{'3', 7, 0, 0},
  {'4', 4, 0, 0},
  {'5', 5, 0, 0},
  {'6', 11, 0, 0},
  {'7', 10, 0, 0},
};

#define IN_MAPPING_DIG_LEN 32
struct in_mapping_dig in_pin_mapping_digital[IN_MAPPING_DIG_LEN] = {
  {'a', 37, 0, true},
  {'b', 36, 0, true},
  {'c', 35, 0, true},
  {'d', 34, 0, true},
  {'e', 33, 0, true},
  {'f', 32, 0, true},

  {'g', 23, 0, true},
  {'h', 24, 0, true},
  {'i', 25, 0, true},
  {'j', 26, 0, true},
  {'k', 27, 0, true},
  {'l', 28, 0, true},

  {'m', 15, 0, true},
  {'n', 16, 0, true},
  {'o', 17, 0, true},
  {'p', 18, 0, true},
  {'q', 19, 0, true},
  {'r', 45, 0, true},

  {'s', 39, 0, true},
  {'t', 40, 0, true},
  {'u', 41, 0, true},
  {'v', 42, 0, true},
  {'w', 43, 0, true},
  {'x', 44, 0, true},

  {'8', 67, 0, true},
  {'9', 66, 0, true},

  {':', 29, 0, true},
  {';', 30, 0, true},

  {'<', 31, 0, true},
  {'=', 38, 0, true},

  {'>', 69, 0, true},
  {'?', 68, 0, true},
};

const char* mqtt_server = "192.168.2.1";

unsigned long time_100ms;
unsigned long time_20ms;

EthernetClient ethClient;
PubSubClient client(ethClient);

long lastMsg = 0;
char msg[50];
int value = 0;

char* SwitchSernum = "D0000";
char* outTopic = "D0000/+/state";
char* inTopic = "D0000/+/command";


void SetBlueStatus(bool state)
{
  static uint8_t cnt = 0;
  if (state == STATUS_OFF)
  {
    digitalWrite(BLUE_STATUS, STATUS_OFF);
    cnt = 0;
  }
  else if (cnt == 0)
  {
    cnt = 1;

  }
  else if (cnt == 1)
  {
    digitalWrite(BLUE_STATUS, STATUS_ON);
    cnt = 2;
  }

}

void handlePin(String message, String title, struct out_mapping output_pin)
{
  if (title.indexOf(output_pin.name) != -1)
  {
    if (message.indexOf("ON") != -1)
    {
      Publish(output_pin.name, "ON", true);
      digitalWrite(output_pin.pin_nr, HIGH);
    }
    else if (message.indexOf("OFF") != -1)
    {
      Publish(output_pin.name, "OFF", true);
      digitalWrite(output_pin.pin_nr, LOW);
    }
  }
}



void callback(char* topic, byte* payload, unsigned int length) {
  SetBlueStatus(STATUS_OFF);
  String title = "";
  for (int i = 0; i < 15; i++) {
    title += topic[i];
  }

  String message = "";
  for (int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  title = title.substring(6);
  title = title.substring(0, 1);

  for (int i = 0; i < OUT_MAPPING_LEN; i++)
  {
    handlePin(message, title, out_pin_mapping[i]);
  }

  if (message.indexOf("ABLE") != -1)
  {
    for (int i = 0; i < IN_MAPPING_DIG_LEN; i++)
    {
      if (title.indexOf(in_pin_mapping_digital[i].name) != -1)
      {
        in_pin_mapping_digital[i].activated = (message.indexOf("ENA") != -1);
      }
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  static bool status_led = false;
  if (!client.connected()) {
    // Attempt to connect
    Serial.println("Trying to connect...");
    status_led = !status_led;
    digitalWrite(BLUE_STATUS, status_led);
    if (client.connect(SwitchSernum)) { //clientId
      Serial.println(inTopic);
      client.subscribe(inTopic);
      Serial.println("Connected");
      client.publish("newdevice", SwitchSernum);
    }
  }
}

void Publish(char suffix, char payload[], bool retain)
{
  SetBlueStatus(STATUS_OFF);
  outTopic[6] = suffix;

  client.publish(outTopic, payload, retain);
}

void setup()
{
  //Initialize Serial communication
  Serial.begin(9600);

  //Initialize Status led
  pinMode(GREEN_STATUS, OUTPUT);
  pinMode(BLUE_STATUS, OUTPUT);
  digitalWrite(BLUE_STATUS, STATUS_OFF);

  bool green_status_led = true;
  for (int i = 0; i < 20; i++)
  {
    green_status_led = !green_status_led;
    digitalWrite(GREEN_STATUS, green_status_led);
    delay(50);
  }


  //Read Serial number from EEPROM
  for (int i = 0; i < 5; i++)
  {
    SwitchSernum[i] = EEPROM.read(i);
  }

  //Set MQTT input and OUTPUT topic's
  (String(SwitchSernum) + "/+/command").toCharArray(inTopic, 17);
  (String(SwitchSernum) + "/+/state").toCharArray(outTopic, 15);

  //Create MAC adress from Serial number
  int intSwitchSernum = String(SwitchSernum).substring(1).toInt();
  byte mac[] = {
    0xDE, 0xAD, 0xBE, 0xEF, (intSwitchSernum / 100), (intSwitchSernum % 100)
  };

  digitalWrite(GREEN_STATUS, STATUS_OFF);
  //Wait for the DHCP to give an ip adress
  IPAddress nonValidIp(0, 0, 0, 0);
  int count = 0;
  do
  {

    count++;
    Ethernet.begin(mac);
    Serial.println(Ethernet.localIP());
    if (count == 10)
    {
      wdt_enable(WDTO_500MS);
    }
  }
  while (Ethernet.localIP() == nonValidIp);


  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  digitalWrite(GREEN_STATUS, STATUS_ON);

  Serial.println(Ethernet.localIP());

  //Init Dig Output pins
  for (int i = 0; i < OUT_MAPPING_LEN; i++)
  {
    pinMode(out_pin_mapping[i].pin_nr, OUTPUT);
  }

  //Read initial pin state
  for (int i = 0; i < IN_MAPPING_DIG_LEN; i++)
  {
    pinMode(in_pin_mapping_digital[i].pin_nr, INPUT);
    in_pin_mapping_digital[i].prevVal = digitalRead(in_pin_mapping_digital[i].pin_nr);
  }

  Serial.println("end setup");

}

void loop()
{
  if (millis() > time_100ms + 100)
  {
    time_100ms = millis() + 100;
    loop_100ms();
  }

  if (millis() > time_20ms + 20)
  {
    time_20ms = millis() + 20;
    loop_20ms();
  }
}

void loop_100ms()
{
  if (!client.connected()) {
    reconnect();
    wdt_enable(WDTO_500MS);
  }
  else {
    wdt_reset();
    SetBlueStatus(STATUS_ON);
  }
  client.loop();

  for (int i = 0; i < IN_MAPPING_DIG_LEN; i++)
    readPin(&in_pin_mapping_digital[i]);


  for (int i = 0; i < IN_MAPPING_ANA_LEN; i++)
    readAnalog(&in_pin_mapping_analog[i]);

}

void loop_20ms()
{

}

//Read an Analog pin and if it is changed, report over mqtt in a value between 0 - 10
void readAnalog(struct in_mapping_ana *input_pin)
{
  char val = max(0, min(9, map(analogRead(input_pin->pin_nr), 300, 950, 9, 0))) + 48;
  Serial.println(val);
  if (input_pin->prevVal != val)
  {
    input_pin->cnt = input_pin->cnt + 1;
    if (input_pin->cnt > 10)
    {
      char *tmp = "0 ";
      tmp[0] = val;
      Publish(input_pin->name, tmp, true);

      input_pin->prevVal = val;
    }

  }
  else
    input_pin->cnt = 0;
}

//Read a Digital pin and if it is changed, report over mqtt
void readPin(struct in_mapping_dig *input_pin)
{
  if (input_pin->activated)
  {
    bool curstate = digitalRead(input_pin->pin_nr);
    if (input_pin->prevVal != curstate)
    {
      input_pin->prevVal = curstate;
      if (curstate)
        Publish(input_pin->name, "ON", false);
      else
        Publish(input_pin->name, "OFF", false);
    }
  }
}
